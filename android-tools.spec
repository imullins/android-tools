%global debug_package %{nil}

Name:          android-tools
Version:       33.0.3p1
Release:       1%{?dist}
Epoch:         1
Summary:       Android platform tools

# The entire source code is ASL 2.0 except boringssl which is BSD
License:       ASL 2.0 and (ASL 2.0 and BSD)
URL:           http://developer.android.com/guide/developing/tools/

#  Sources with all needed patches and cmakelists live there now:
Source0:       https://github.com/nmeum/%{name}/releases/download/%{version}/%{name}-%{version}.tar.xz
Patch0:        Disable-dependency-heavy-tools.patch

BuildRequires: cmake
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: zlib-devel

%description
Minimal version of the android-tools package.
Only essential tools are installed from the source code.
avbtool, mkbootimg and sparse image tools.

%prep
%autosetup -p1

%build
%cmake -DBUILD_SHARED_LIBS:BOOL=OFF
%cmake_build

%install
%cmake_install

%files
%attr(0755,root,root) %dir %{_datadir}/android-tools
%{_bindir}/avbtool
%{_bindir}/simg2img
%{_bindir}/img2simg
%{_bindir}/append2simg
%{_bindir}/mkbootimg
%{_bindir}/mkdtboimg
%{_bindir}/repack_bootimg
%{_bindir}/unpack_bootimg
%{_datadir}/android-tools/mkbootimg/gki/generate_gki_certificate.py
%{_datadir}/android-tools/mkbootimg/mkbootimg.py

%changelog
* Fri Oct 20 2023 Ian Mullins <imullins@redhat.com> - 33.0.3p1-1
- Use classic changelog, as the autochangelog macro is not widely supported outside of Fedora.
- Use classic release, as the autorelease macro is not widely supported outside of Fedora.
- Apply Disable-dependency-heavy-tools.patch. The Complete android-tools
- package requires additional dependencies to build all tools provided.
- This patch disables additional tools from the build.
- Remove unused buildrequires.
